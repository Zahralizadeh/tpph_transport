package com.tpph.transport;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Q & Z on 2/4/2018.
 */

public class BaseActivity extends AppCompatActivity {
    public Context mContext = this ;
    public Activity mActivity = this ;
}
