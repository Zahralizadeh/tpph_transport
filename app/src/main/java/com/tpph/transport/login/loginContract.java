package com.tpph.transport.login;

import com.tpph.transport.webModels.LoginBacktoryPojoModel;

/**
 * Created by Q & Z on 2/10/2018.
 */

public interface loginContract {
    interface View {
        void FailureLoginShow(String error);
        void openDrivers();
    }

    interface Presenter {
        void attachView(View view);
        void authenticateUser(String username, String password);
        void onSuccessLoginToBacktory();
        void onFailureLoginToBacktory(String errorMassage);
    }

    interface Model {
        void attachPresenter (Presenter presenter);
        void loginToBacktory(String username, String password);
    }
}
