package com.tpph.transport.login;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;
import com.tpph.transport.Constants;
import com.tpph.transport.LoginActivity;
import com.tpph.transport.MainActivity;
import com.tpph.transport.webModels.LoginBacktoryPojoModel;

import cz.msebera.android.httpclient.Header;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zahra on 2/10/2018.
 * Webservices are integrated with RX
 */

public class loginModel implements loginContract.Model {
    private loginContract.Presenter presenter;

    @Override
    public void attachPresenter(loginContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void loginToBacktory(String username, String password) {
            Constants.webLoginUtil.login(username,password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<LoginBacktoryPojoModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(LoginBacktoryPojoModel loginBacktoryPojoModel) {
                            Log.d("zahra", "onLoginSuccess" +loginBacktoryPojoModel.getAccessToken());
                            saveTokenPreShared(loginBacktoryPojoModel);
                            presenter.onSuccessLoginToBacktory();

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("zahra onFailure", "onLoginFailure");
                            presenter.onFailureLoginToBacktory(e.getMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

//                    enqueue(new Callback<LoginBacktoryPojoModel>() {
//                @Override
//                public void onResponse(Call<LoginBacktoryPojoModel> call, Response<LoginBacktoryPojoModel> response) {
//                    Log.d("zahra", "onLoginSuccess" +response.body().getAccessToken());
//                saveTokenPreShared(response.body());
//                presenter.onSuccessLoginToBacktory();
//                }
//                @Override
//                public void onFailure(Call<LoginBacktoryPojoModel> call, Throwable t) {
//                    Log.d("zahra onFailure", "onLoginFailure");
//                presenter.onFailureLoginToBacktory(t.getMessage());
//                }
//            });
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.addHeader("X-Backtory-Authentication-Id", Constants.X_Backtory_Authentication_Id);
//        client.addHeader("X-Backtory-Authentication-Key", Constants.X_Backtory_Authentication_Key);
//        RequestParams params = new RequestParams();
//        params.put("username", username);
//        params.put("password", password);
//
//        client.post(Constants.loginURL, params, new TextHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Log.d("zahra onFailure", "onFailure:" + responseString);
//                presenter.onFailureLoginToBacktory(responseString);
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                Log.d("zahra", "onSuccess:" + responseString);
//                LoginBacktoryPojoModel token = getTocken((responseString));
//                saveTockenPreShared(token);
//                presenter.onSuccessLoginToBacktory();
//            }
//        });
    }

    private void saveTokenPreShared(LoginBacktoryPojoModel token) {
        Hawk.put("accessToken", token.getAccessToken());
        Hawk.put("refreshToken", token.getRefreshToken());
    }
//
//    private LoginBacktoryPojoModel getTocken(String jsonStr) {
//        Gson gson = new Gson();
//        return gson.fromJson(jsonStr, LoginBacktoryPojoModel.class);
//    }
}
