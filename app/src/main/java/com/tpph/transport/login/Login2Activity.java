package com.tpph.transport.login;

import android.content.Intent;
import android.widget.EditText;

import com.tpph.transport.BaseActivity;
import com.tpph.transport.HomeMap.HomeMapActivity;
import com.tpph.transport.PublicMethods;
import com.tpph.transport.R;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login2)
public class Login2Activity extends BaseActivity implements loginContract.View {
    LoginPresenter presenter = new LoginPresenter();

    @ViewById
    EditText username;
    @ViewById
    EditText password;

    @AfterViews
    void initView() {
        presenter.attachView(this);

    }

    @Click
    void loginbtn() {
        presenter.authenticateUser(username.getText().toString(), password.getText().toString());
    }

    @Override
    public void FailureLoginShow(String error) {
        PublicMethods.showToast(mContext, error);
    }

    @Override
    public void openDrivers() {
//        DriversActivity_.intent(mContext).start();
        Intent intent = new Intent(Login2Activity.this, HomeMapActivity.class);
        startActivity(intent);
    }

}
