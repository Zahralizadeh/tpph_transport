package com.tpph.transport.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.tpph.transport.BaseActivity
import com.tpph.transport.HomeMap.HomeMap2Activity
import com.tpph.transport.HomeMap.HomeMapActivity
import com.tpph.transport.PublicMethods
import com.tpph.transport.R
import com.tpph.transport.utils.views.myButton
import com.tpph.transport.utils.views.myEditText
import com.tpph.transport.utils.views.myTextView
import kotlinx.android.synthetic.main.activity_login2.*

//third version of login activity!
//this time in KOTLIN :)
class login3Activity : BaseActivity(), loginContract.View {

    var presenter: LoginPresenter = LoginPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login2)
        loginbtn.setOnClickListener(View.OnClickListener {
            presenter.authenticateUser(username.text.toString(), password.text.toString())
        })
        presenter.attachView(this)
    }

    override fun FailureLoginShow(error: String?) {
        PublicMethods.showToast(mContext, error)
    }

    override fun openDrivers() {
        val intent = Intent(mActivity, HomeMap2Activity::class.java)
        startActivity(intent)
    }

}
