package com.tpph.transport.login;

import com.orhanobut.hawk.Hawk;
import com.tpph.transport.PublicMethods;
import com.tpph.transport.webModels.LoginBacktoryPojoModel;

/**
 * Created by Q & Z on 2/10/2018.
 */

public class LoginPresenter implements loginContract.Presenter {
    private loginContract.View view;
    private loginModel model = new loginModel();

    @Override
    public void attachView(loginContract.View view) {
        this.view = view;
        model.attachPresenter(this);
        stayedSigninCheck();
    }

    private void stayedSigninCheck() {
        if( Hawk.get("accessToken")!=null)
            view.openDrivers();
    }

    @Override
    public void authenticateUser(String username, String password) {
        model.loginToBacktory(username, password);
    }

    @Override
    public void onSuccessLoginToBacktory() {
        view.openDrivers();
    }

    @Override
    public void onFailureLoginToBacktory(String error) {
        view.FailureLoginShow(error);
    }
}

