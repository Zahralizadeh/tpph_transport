package com.tpph.transport;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.tpph.transport.webModels.LoginBacktoryPojoModel;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText txtUsername = (EditText) findViewById(R.id.username);
        final EditText txtPassword = (EditText) findViewById(R.id.password);

        findViewById(R.id.loginbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameSTR = txtUsername.getText().toString();
                String passSTR = txtPassword.getText().toString();
                Log.d("username", usernameSTR);
                Log.d("password", passSTR);
                PublicMethods.showToast(mContext, usernameSTR);
                loginToBacktory(usernameSTR, passSTR);
            }
        });
    }

    private void loginToBacktory(String username, String password) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("X-Backtory-Authentication-Id", Constants.X_Backtory_Authentication_Id);
        client.addHeader("X-Backtory-Authentication-Key", Constants.X_Backtory_Authentication_Key);
        RequestParams params = new RequestParams();
        params.put("username", username);
        params.put("password", password);

        client.post(Constants.loginURL, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("zahra onFailure", "onFailure:" + responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("zahra", "onSuccess:" + responseString);
                saveAccessTockenPreShared(responseString);
                Intent openMainActivity = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(openMainActivity);
            }
        });
    }

    private void saveAccessTockenPreShared(String responseString) {
        LoginBacktoryPojoModel tocken = getTocken(responseString);
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().
                putString("accessTocken",tocken.getAccessToken()).apply();
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("refreshTocken",
                tocken.getRefreshToken()).apply();
        Log.d("zahra accessTocken", tocken.getAccessToken());
        Log.d("zahra refreshTocken", tocken.getRefreshToken());
        String tockenCheck = PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString("accessTocken", "");
        Log.d("zahra authtocken", tockenCheck);
    }

    private LoginBacktoryPojoModel getTocken(String jsonStr) {
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, LoginBacktoryPojoModel.class);
    }
}
