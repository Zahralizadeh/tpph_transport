package com.tpph.transport.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import com.tpph.transport.myApplication;

/**
 * Created by Q & Z on 2/12/2018.
 */

public class myTextView extends AppCompatTextView {
    public myTextView(Context context) {
        super(context);
    }

    public myTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(myApplication.font);
    }

    public void text(String keyword) {
        this.setText(keyword);

    }
}
