package com.tpph.transport.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import com.tpph.transport.myApplication;

/**
 * Created by zahra on 3/27/2018.
 */

public class myButton extends AppCompatButton {
    public myButton(Context context) {
        super(context);
    }

    public myButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(myApplication.font);
    }
}
