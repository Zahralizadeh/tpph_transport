package com.tpph.transport.utils;

import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel;
import com.tpph.transport.webModels.DriversListPojoModel;
import com.tpph.transport.webModels.LoginBacktoryPojoModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Q & Z on 2/18/2018.
 */

public interface WebInterFace {

    @FormUrlEncoded
    @POST("auth/login")
    Observable<LoginBacktoryPojoModel> login(
            @Field("username") String username ,
            @Field("password") String password
    );

    @POST("object-storage/classes/query/Drivers")
    Observable<String> getDriversList();

    @POST ("object-storage/classes/query/Drivers?keys=name,family,carModel,PicURL,PersonelNO," +
            "LastLongitude,LastLatitude,CarColor,_id")
        Observable<DriversSummeryPojoModel> getDriversSummery();
}
