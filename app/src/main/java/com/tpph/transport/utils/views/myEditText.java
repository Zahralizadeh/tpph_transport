package com.tpph.transport.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.tpph.transport.myApplication;

/**
 * Created by Q & Z on 2/11/2018.
 */

public class myEditText extends AppCompatEditText {
    public myEditText(Context context) {
        super(context);
    }

    public myEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(myApplication.font);
    }

    public String text(){
        return this.getText().toString();

    }
}
