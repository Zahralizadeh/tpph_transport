package com.tpph.transport.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.squareup.picasso.Picasso;

/**
 * Created by Q & Z on 2/12/2018.
 */

public class myImageView extends AppCompatImageView {

    Context mContext;

    public myImageView(Context context) {
        super(context);
        mContext = context;
    }

    public myImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void load(String url) {
        Picasso.with(mContext).load(url).into(this);
    }
}
