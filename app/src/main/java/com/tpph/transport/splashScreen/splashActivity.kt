package com.tpph.transport.splashScreen


import android.os.Bundle
import com.tpph.transport.BaseActivity
import com.tpph.transport.R
import android.content.Intent
import android.os.Handler
import android.support.v4.app.ActivityOptionsCompat
import com.tpph.transport.login.login3Activity
import kotlinx.android.synthetic.main.activity_login2.*

class splashActivity : BaseActivity() {
    private var backbtnPress: Boolean = false
    private val SPLASH_DURATION: Long = 5000
    private var myHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        myHandler = Handler()
        myHandler!!.postDelayed(Runnable {
            finish()
            if (!backbtnPress) {
                val intent = Intent(mActivity, login3Activity::class.java)
                val optionCompat : ActivityOptionsCompat = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(mActivity,logo,"logo_animation")
                startActivity(intent,optionCompat.toBundle())
                overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout)
            }
        }, SPLASH_DURATION)
    }

    override fun onBackPressed() {
        backbtnPress = true
        super.onBackPressed()
    }

}
