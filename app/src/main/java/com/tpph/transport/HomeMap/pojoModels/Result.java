
package com.tpph.transport.HomeMap.pojoModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("CarColor")
    @Expose
    private String carColor;
    @SerializedName("PicURL")
    @Expose
    private String picURL;
    @SerializedName("carModel")
    @Expose
    private String carModel;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("PersonelNO")
    @Expose
    private Integer personelNO;
    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("LastLatitude")
    @Expose
    private Double lastLatitude;
    @SerializedName("LastLongitude")
    @Expose
    private Double lastLongitude;
    @SerializedName("ACL")
    @Expose
    private ACL aCL;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPersonelNO() {
        return personelNO;
    }

    public void setPersonelNO(Integer personelNO) {
        this.personelNO = personelNO;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Double getLastLatitude() {
        return lastLatitude;
    }

    public void setLastLatitude(Double lastLatitude) {
        this.lastLatitude = lastLatitude;
    }

    public Double getLastLongitude() {
        return lastLongitude;
    }

    public void setLastLongitude(Double lastLongitude) {
        this.lastLongitude = lastLongitude;
    }

    public ACL getACL() {
        return aCL;
    }

    public void setACL(ACL aCL) {
        this.aCL = aCL;
    }

}
