package com.tpph.transport.HomeMap

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.*
import com.google.android.gms.maps.GoogleMap
import android.text.Html
import android.util.Log
import android.view.View
import com.google.android.gms.maps.model.Marker
import com.tpph.transport.R
import com.tpph.transport.utils.views.myImageView
import com.tpph.transport.utils.views.myTextView
import android.text.style.UnderlineSpan
import android.text.SpannableString
import android.widget.TextView
import com.tpph.transport.PublicMethods.getString
import com.tpph.transport.driverDetails.driverDetailActivity
import com.tpph.transport.login.Login2Activity


/**
 * Created by zahra on 3/28/2018.
 */
class myCustomInfoWindowGoogleMap(private val context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    @SuppressLint("SetTextI18n")
    override fun getInfoContents(marker: Marker): View {
        @SuppressLint("InflateParams") val view = (context as Activity).layoutInflater
                .inflate(R.layout.map_info_window, null)
        val name = view.findViewById<myTextView>(R.id.name)
        val img = view.findViewById<myImageView>(R.id.pic)
        val personnelNO = view.findViewById<myTextView>(R.id.pesonnelNO)
        val car = view.findViewById<myTextView>(R.id.car)
        val link = view.findViewById<myTextView>(R.id.link)

        val content = SpannableString(getString(context, R.string.driver_Details))
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        link.text(content.toString())

//        link.text = R.string.driver_Details.toString()
        name.text = marker.title
        val info = marker.tag as myInfoWindowData?
        personnelNO.text = info?.personnelNO
        car.text = info?.car
        img.load(info?.image)

        Log.d("imgCheck", "showDriversOnMap: " + info?.image)
        Log.d("carCheck", "showDriversOnMap: " + info?.car)
        Log.d("personelCheck", "showDriversOnMap: " + info?.personnelNO)

        link.setOnClickListener {
            val intent = Intent(context, driverDetailActivity::class.java)
            intent.putExtra("personnel", info?.personnelNO)
            startActivity(context,intent,null)
        }
        return view
    }
}


