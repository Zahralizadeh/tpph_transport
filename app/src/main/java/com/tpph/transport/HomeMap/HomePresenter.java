package com.tpph.transport.HomeMap;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.Marker;
import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel;
import com.tpph.transport.R;
import com.tpph.transport.driverDetails.driverDetailActivity;
import com.tpph.transport.webModels.DriversListPojoModel;

/**
 * Created by zahra on 3/25/2018.
 */

public class HomePresenter implements HomeContract.Presenter {
    private HomeContract.View view;
    HomeModel model = new HomeModel();

    @Override
    public void attachView(HomeContract.View view) {
        this.view = view;
        model.attachPresenter(this);
        model.getDriversList();
    }

    @Override
    public void onFailureLoadDrivers(String response) {
        if (response.equalsIgnoreCase("invalid_token")) {
            view.showToast(R.string.errorInvalidToken);
        } else {
            view.showToast(R.string.errorConnectingToServers);
        }
        view.showDriversOnMap( model.loadDriversSummeryFromDB());

    }

    @Override
    public void onSuccessLoadDrivers(DriversListPojoModel drivers) {
        view.showDriversOnMap(drivers);
    }

@Override
    public void onSuccessLoadDriversSummery(DriversSummeryPojoModel drivers) {
        view.showDriversOnMap(drivers);
        model.saveDriverSummeryInDatabase(drivers);

    }

    @Override
    public void clickOnMarker(Marker marker, Context context) {
        view.goToDriverDetailActivity(model.getPersonnelNO(marker,context));
    }
}
