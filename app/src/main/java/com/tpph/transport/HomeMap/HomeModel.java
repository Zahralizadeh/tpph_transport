package com.tpph.transport.HomeMap;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.orhanobut.hawk.Hawk;
import com.tpph.transport.Constants;
import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel;
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm;
import com.tpph.transport.PublicMethods;
import com.tpph.transport.R;
import com.tpph.transport.webModels.DriversListPojoModel;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zahra on 3/25/2018.
 */

public class HomeModel implements HomeContract.Model {

    private HomeContract.Presenter presenter;
    Realm realm = Realm.getDefaultInstance();

    @Override
    public void attachPresenter(HomeContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public void getDriversList() {
        Constants.webDriverListUtil.getDriversSummery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DriversSummeryPojoModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(DriversSummeryPojoModel drivers) {
                        presenter.onSuccessLoadDriversSummery(drivers);
                        Log.d("webservice", "OnSuccess: driverlistsize is: " + drivers.getResults().size());
                    }

                    @Override
                    public void onError(Throwable e) {
                        presenter.onFailureLoadDrivers(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
//       ******* OLD implementation by JUST Retrofit*******
//        Constants.webDriverListUtil.getDriversList().enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                String responseStr = RepairACLstar(response.body());
//                Log.d("Driver", responseStr);
//                DriversListPojoModel drivers = getDriverList(responseStr);
//                presenter.onSuccessLoadDrivers(drivers);
//            }

//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                presenter.onFailureLoadDrivers(t.getMessage());
//            }
//        });
    }

//    private void OnSuccess(DriversSummeryPojoModel drivers) {
//        presenter.onSuccessLoadDriversSummery(drivers);
//        Log.d("webservice", "OnSuccess: driverlistsize is: "+drivers.getResults().size());
//    }

//    private void OnSuccess(String response) {
//        String responseStr = RepairACLstar(response);
//                Log.d("Driver", responseStr);
//                DriversListPojoModel drivers = getDriverList(responseStr);
//                presenter.onSuccessLoadDrivers(drivers);
//    }

    private void onFailure(Throwable t) {
        presenter.onFailureLoadDrivers(t.getMessage());
    }


    @Override
    public String getPersonnelNO(Marker marker, Context context) {
        myInfoWindowData info = (myInfoWindowData) marker.getTag();
        String str = info.getPersonnelNO();
        try {
            if (str != null) {
                return str.replaceFirst(PublicMethods.getString(context, R.string.personnelNO_title), "");
            }
        } catch (Exception e) {
            Log.d("HOMEmodel_Exception", "getPersonnelNO: " + e.toString());
        }
        return str;
    }

    @Override
    public void saveDriverSummeryInDatabase(DriversSummeryPojoModel driverPojo) {
        Log.d("Realm", "saveDriverSummeryInDatabase: Hello!");
        if (driverPojo.getResults() != null) {
            realm.beginTransaction();
            realm.delete(DriverModelRealm.class);
            realm.commitTransaction();
            for (int i = 0; i < driverPojo.getResults().size(); i++) {
                realm.beginTransaction();
                DriverModelRealm driver = realm.createObject(DriverModelRealm.class,driverPojo.getResults().get(i).getId());
                driver.setCarColor(driverPojo.getResults().get(i).getCarColor());
                driver.setCarModel(driverPojo.getResults().get(i).getCarModel());
                driver.setFamily(driverPojo.getResults().get(i).getFamily());
                driver.setName(driverPojo.getResults().get(i).getName());
                driver.setLastLatitude(driverPojo.getResults().get(i).getLastLatitude());
                driver.setLastLongitude(driverPojo.getResults().get(i).getLastLongitude());
                driver.setPersonelNO(driverPojo.getResults().get(i).getPersonelNO());
                driver.setPicURL(driverPojo.getResults().get(i).getPicURL());
                realm.copyToRealm(driver);
                realm.commitTransaction();
                Log.d("Realm", "saveDriverSummeryInDatabase: " + driverPojo.getResults().get(0).getId());
            }
        } else
            Log.d("Realm", "driver pojo is null ");

    }

    @Override
    public List<DriverModelRealm> loadDriversSummeryFromDB() {
        return realm.where(DriverModelRealm.class).findAll();
    }

    private String RepairACLstar(String responseString) {
        return responseString.replace("*", "All");
    }

    private DriversListPojoModel getDriverList(String jsonStr) {
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, DriversListPojoModel.class);
    }

    @Override
    public String getToken(String key) {
        return Hawk.get(key);
    }
}
