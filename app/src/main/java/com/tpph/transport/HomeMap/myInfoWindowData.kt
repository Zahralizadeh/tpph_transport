package com.tpph.transport.HomeMap

/**
 * Created by zahra on 3/28/2018.
 */
data class myInfoWindowData(
        var name: String?,
        var image: String?,
        var personnelNO : String?,
        var car :String?
)