package com.tpph.transport.HomeMap.pojoModels.RealmModels

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

public open class DriverModelRealm() : RealmObject() {
    @PrimaryKey
    var id: String? = null
    var name: String? = null
    var family: String? = null
    var personelNO: Int? = null
    var picURL: String? = null
    var updatedAt: String? = null
    var carModel: String? = null
    var carColor: String? = null
    var lastLatitude: Double? = null
    var lastLongitude: Double? = null


//    var attendances: List<Attendance>? = null

}

