package com.tpph.transport.HomeMap

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm
import com.tpph.transport.PublicMethods
import com.tpph.transport.R
import com.tpph.transport.driverDetails.driverDetailActivity
import com.tpph.transport.webModels.DriversListPojoModel
import com.tpph.transport.HomeMap.pojoModels.Result
import com.tpph.transport.adapters.driversListAdapter
import kotlinx.android.synthetic.main.activity_home_map.*

class HomeMap2Activity : FragmentActivity(), OnMapReadyCallback, HomeContract.View {


    private var mMap: GoogleMap? = null
    internal var presenter: HomePresenter = HomePresenter()
    internal var markerDoubleClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        presenter.attachView(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.getUiSettings()?.isZoomControlsEnabled = true
        addHomeMarkers()
    }

    private fun addHomeMarkers() {
        // Add a marker for branches of the company and move the camera
        val tpph = LatLng(35.764586, 51.345410)
        //First Try your Current LOCATION, IF IT WAS NOT AVAILABLE THEN TRY PRE DEFIEND LOCATION
        mMap?.addMarker(MarkerOptions().position(tpph).title(PublicMethods.getString(this, R.string.tpph_name))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.tpph)))
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(tpph, 13f))
    }

    override fun showDriversOnMap(drivers: DriversSummeryPojoModel?) {
        var driverLatLang: LatLng
        var info: myInfoWindowData
        var m: Marker

        if (drivers?.getResults()?.size!! > 0) {
            var customInfoWindow: myCustomInfoWindowGoogleMap
            for (i in 0 until drivers.getResults().size) {
                driverLatLang = LatLng(drivers.getResults()[i].lastLatitude!!,
                        drivers.getResults()[i].lastLongitude!!)

                info = myInfoWindowData(
                        getDriverName(drivers.getResults()[i]),
                        drivers.getResults()[i].picURL,
                        PublicMethods.getString(this, R.string.personnelNO_title)
                                + drivers.getResults()[i].personelNO!!,
                        PublicMethods.getString(this, R.string.car_title)
                                + drivers.getResults()[i].carModel + " "
                                + drivers.getResults()[i].carColor)

                customInfoWindow = myCustomInfoWindowGoogleMap(this)
                mMap?.setInfoWindowAdapter(customInfoWindow)

                m = mMap?.addMarker(MarkerOptions()
                        .position(driverLatLang)
                        .title(getDriverName(drivers.getResults()[i]))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)))!!
                m.tag = info
                m.showInfoWindow()
            }
            mMap?.setOnInfoWindowClickListener(GoogleMap.OnInfoWindowClickListener { marker -> presenter.clickOnMarker(marker, this) })
        }
        loadMenu(drivers)
    }

    override fun showDriversOnMap(drivers: DriversListPojoModel?) {
        var driverLatLang: LatLng
        var info: myInfoWindowData
        var m: Marker

        if (drivers?.getResults()?.size!! > 0) {
            var customInfoWindow: myCustomInfoWindowGoogleMap
            for (i in 0 until drivers.getResults().size) {
                driverLatLang = LatLng(drivers.getResults()[i].lastLatitude!!,
                        drivers.getResults()[i].lastLongitude!!)

                info = myInfoWindowData(
                        getDriverName(drivers.getResults()[i]),
                        drivers.getResults()[i].picURL,
                        PublicMethods.getString(this, R.string.personnelNO_title)
                                + drivers.getResults()[i].personelNO!!,
                        PublicMethods.getString(this, R.string.car_title)
                                + drivers.getResults()[i].carModel + " "
                                + drivers.getResults()[i].carColor)

                customInfoWindow = myCustomInfoWindowGoogleMap(this)
                mMap?.setInfoWindowAdapter(customInfoWindow)

                m = mMap?.addMarker(MarkerOptions()
                        .position(driverLatLang)
                        .title(getDriverName(drivers.getResults()[i]))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)))!!
                m.tag = info
                m.showInfoWindow()
            }
            mMap?.setOnInfoWindowClickListener(GoogleMap.OnInfoWindowClickListener { marker -> presenter.clickOnMarker(marker, this) })
        }
        loadMenu(drivers)
    }

    override fun showDriversOnMap(drivers: MutableList<DriverModelRealm>?) {
        var driverLatLang: LatLng
        var info: myInfoWindowData
        var m: Marker

        if (drivers?.size!! > 0) {
            var customInfoWindow: myCustomInfoWindowGoogleMap
            for (i in 0 until drivers.size) {
                driverLatLang = LatLng(drivers.get(i).lastLatitude!!,
                        drivers.get(i).lastLongitude!!)

                info = myInfoWindowData(
                        getDriverName(drivers.get(i)),
                        drivers.get(i).picURL,
                        PublicMethods.getString(this, R.string.personnelNO_title)
                                + drivers.get(i).personelNO!!,
                        PublicMethods.getString(this, R.string.car_title)
                                + drivers.get(i).carModel + " "
                                + drivers.get(i).carColor)

                customInfoWindow = myCustomInfoWindowGoogleMap(this)
                mMap?.setInfoWindowAdapter(customInfoWindow)

                m = mMap?.addMarker(MarkerOptions()
                        .position(driverLatLang)
                        .title(getDriverName(drivers.get(i)))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)))!!
                m.tag = info
                m.showInfoWindow()
            }
            mMap?.setOnInfoWindowClickListener(GoogleMap.OnInfoWindowClickListener { marker -> presenter.clickOnMarker(marker, this) })
        }
        loadMenu(drivers)
    }

    private fun getDriverName(driver: DriverModelRealm): String? {
        return driver.name + " " + driver.family
    }

    /**
     * get complete name for each driver
     *
     * @param driver 's got from each node of Pojo in  showDriversOnMap method
     * @return full name of driver
     */
    private fun getDriverName(driver: Result): String {
        return driver.name + " " + driver.family
    }

    private fun getDriverName(driver: com.tpph.transport.webModels.Result): String {
        return driver.name + " " + driver.family
    }

    override fun showToast(msg: Int) {
        PublicMethods.showToast(this, PublicMethods.getString(this, msg))
    }

    override fun showToast(msg: String?) {
        PublicMethods.showToast(this, msg)
    }

    override fun goToDriverDetailActivity(PersonnelNO: String?) {
        val intent = Intent(this, driverDetailActivity::class.java)
        intent.putExtra("personnel", PersonnelNO)
        startActivity(intent)
    }

    fun loadMenu(drivers: DriversSummeryPojoModel) {
        val adapter = driversListAdapter(this, drivers)
        driversList.adapter = adapter
        val lm = LinearLayoutManager(this)
        driversList.layoutManager = lm
    }

    fun loadMenu(drivers: DriversListPojoModel) {
        val adapter = driversListAdapter(this, drivers)
        driversList.adapter = adapter
        val lm = LinearLayoutManager(this)
        driversList.layoutManager = lm
    }

    fun loadMenu(drivers: List<DriverModelRealm>) {
        val adapter = driversListAdapter(this, drivers)
        driversList.adapter = adapter
        val lm = LinearLayoutManager(this)
        driversList.layoutManager = lm
    }

}
