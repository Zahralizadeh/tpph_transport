package com.tpph.transport.HomeMap;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel;
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm;
import com.tpph.transport.PublicMethods;
import com.tpph.transport.R;
import com.tpph.transport.driverDetails.driverDetailActivity;
import com.tpph.transport.webModels.DriversListPojoModel;
import com.tpph.transport.webModels.Result;

import java.util.List;


public class HomeMapActivity extends FragmentActivity implements OnMapReadyCallback, HomeContract.View {

    private GoogleMap mMap;
    HomePresenter presenter;
    boolean markerDoubleClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        presenter = new HomePresenter();
        presenter.attachView(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        addHomeMarkers();

    }

    @Override
    public void showDriversOnMap(DriversListPojoModel drivers) {
        LatLng driverLatLang;
        myInfoWindowData info;
        Marker m;

        if (drivers.getResults().size() > 0) {
            myCustomInfoWindowGoogleMap customInfoWindow;
            for (int i = 0; i < drivers.getResults().size(); i++) {
                driverLatLang = new LatLng(drivers.getResults().get(i).getLastLatitude(),
                        drivers.getResults().get(i).getLastLongitude());

                info = new myInfoWindowData(
                        drivers.getResults().get(i).getName() + " " + drivers.getResults().get(i).getFamily(),
                        drivers.getResults().get(i).getPicURL(),
                        PublicMethods.getString(this, R.string.personnelNO_title)
                                + drivers.getResults().get(i).getPersonelNO(),
                        PublicMethods.getString(this, R.string.car_title)
                                + drivers.getResults().get(i).getCarModel() + " "
                                + drivers.getResults().get(i).getCarColor());

                customInfoWindow = new myCustomInfoWindowGoogleMap(HomeMapActivity.this);
                mMap.setInfoWindowAdapter(customInfoWindow);

                m = mMap.addMarker(new MarkerOptions()
                        .position(driverLatLang)
                        .title(drivers.getResults().get(i).getName() + " " + drivers.getResults().get(i).getFamily())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)));
                m.setTag(info);
                m.showInfoWindow();
            }
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    presenter.clickOnMarker(marker,HomeMapActivity.this);

                }
            });
        }
    }

    @Override
    public void showDriversOnMap(DriversSummeryPojoModel drivers) {
        LatLng driverLatLang;
        myInfoWindowData info;
        Marker m;

        if (drivers.getResults().size() > 0) {
            myCustomInfoWindowGoogleMap customInfoWindow;
            for (int i = 0; i < drivers.getResults().size(); i++) {
                driverLatLang = new LatLng(drivers.getResults().get(i).getLastLatitude(),
                        drivers.getResults().get(i).getLastLongitude());

                info = new myInfoWindowData(
                        drivers.getResults().get(i).getName() + " " + drivers.getResults().get(i).getFamily(),
                        drivers.getResults().get(i).getPicURL(),
                        PublicMethods.getString(this, R.string.personnelNO_title)
                                + drivers.getResults().get(i).getPersonelNO(),
                        PublicMethods.getString(this, R.string.car_title)
                                + drivers.getResults().get(i).getCarModel() + " "
                                + drivers.getResults().get(i).getCarColor());

                customInfoWindow = new myCustomInfoWindowGoogleMap(HomeMapActivity.this);
                mMap.setInfoWindowAdapter(customInfoWindow);

                m = mMap.addMarker(new MarkerOptions()
                        .position(driverLatLang)
                        .title(drivers.getResults().get(i).getName() + " " + drivers.getResults().get(i).getFamily())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)));
                m.setTag(info);
                m.showInfoWindow();
            }
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    presenter.clickOnMarker(marker,HomeMapActivity.this);

                }
            });
        }

    }

    @Override
    public void showDriversOnMap(List<DriverModelRealm> drivers) {

    }


//    private String getDriverName(Result driver) {
//        return driver.getName() + " " + driver.getFamily();
//    }

    public void showToast(String msg) {
        PublicMethods.showToast(this, msg);
    }

    @Override
    public void showToast(int msg) {
        PublicMethods.showToast(this, PublicMethods.getString(this, msg));
    }

    private void addHomeMarkers() {
        // Add a marker for branches of the company and move the camera
        LatLng tpph = new LatLng(35.764586, 51.345410);
        //First Try your Current LOCATION, IF IT WAS NOT AVAILABLE THEN TRY PRE DEFIEND LOCATION
        mMap.addMarker(new MarkerOptions().position(tpph).title(PublicMethods.getString(this, R.string.tpph_name))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.tpph)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tpph, 13));
    }

    public void goToDriverDetailActivity(String PersonnelNO) {
        Intent intent = new Intent(HomeMapActivity.this, driverDetailActivity.class);
        intent.putExtra("personnel", PersonnelNO);
        startActivity(intent);
    }

}
