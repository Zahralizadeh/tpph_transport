package com.tpph.transport.HomeMap;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.Marker;
import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel;
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm;
import com.tpph.transport.webModels.DriversListPojoModel;

import java.util.List;

/**
 * Created by zahra on 3/25/2018.
 */

public interface HomeContract {
    interface View {
        void showDriversOnMap(DriversListPojoModel drivers);
        void showDriversOnMap(DriversSummeryPojoModel drivers);
        void showDriversOnMap(List<DriverModelRealm> drivers);
        void showToast(int msg);
        void showToast(String msg);
        void goToDriverDetailActivity(String markerTitle);
    }

    interface Presenter {
        void attachView(View view);
        void onFailureLoadDrivers(String response);
        void onSuccessLoadDrivers(DriversListPojoModel drivers);
        void onSuccessLoadDriversSummery(DriversSummeryPojoModel drivers);
        void clickOnMarker(Marker marker,Context context);

    }

    interface Model {
        void attachPresenter(Presenter presenter);
        String getToken(String key);
        void getDriversList();
        String getPersonnelNO(Marker marker,Context context);
        void saveDriverSummeryInDatabase(DriversSummeryPojoModel drivers);
        List<DriverModelRealm> loadDriversSummeryFromDB();

    }
}
