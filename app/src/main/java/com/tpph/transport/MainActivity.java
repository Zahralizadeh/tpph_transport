package com.tpph.transport;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.tpph.transport.webModels.DriversListPojoModel;
import com.tpph.transport.webModels.LoginBacktoryPojoModel;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        RecyclerView driversLst = (RecyclerView) findViewById(R.id.driversList);
        loadDriversList();

    }

    private void loadDriversList() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", getTocken());
        client.addHeader("X-Backtory-Object-Storage-Id", Constants.X_Backtory_Object_Storage_Id);
        client.addHeader("Content-Type", "application/json");
        RequestParams params = new RequestParams();

        client.post(Constants.queryDriverURL, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.showToast(mContext, responseString);
                Log.d("zahra Drivers onFailure", responseString);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                PublicMethods.showToast(mContext, "OK");
                responseString = RepairACLstar(responseString);
                DriversListPojoModel drivers = getDriverList(responseString);
                PublicMethods.showToast(mContext,drivers.getResults().get(0).getFamily());
            }
        });
    }

    private String getTocken() {
        String tocken = PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString("accessTocken", "noTocken");
        Log.d("zahra authtocken", tocken);
        return "bearer " + tocken;
    }

    private String RepairACLstar(String responseString) {
        responseString = responseString.replace("*", "All");
        Log.d("zahra RepairACLstar: ", responseString);
        return responseString;
    }

    private DriversListPojoModel getDriverList(String jsonStr) {
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, DriversListPojoModel.class);

    }


}
