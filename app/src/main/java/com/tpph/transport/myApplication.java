package com.tpph.transport;

import android.app.Application;
import android.graphics.Typeface;

import com.orhanobut.hawk.Hawk;

import io.realm.Realm;

/**
 * Created by Q & Z on 2/10/2018.
 */

public class myApplication extends Application {
    public static Typeface font;
    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
        font = Typeface.createFromAsset(getAssets(),Constants.defaultFont);
        Realm.init(this);
    }
}
