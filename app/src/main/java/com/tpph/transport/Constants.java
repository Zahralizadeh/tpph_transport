package com.tpph.transport;

import com.tpph.transport.utils.RetrofitServiceGenerator;
import com.tpph.transport.utils.WebInterFace;

/**
 * Created by Q & Z on 2/4/2018.
 */

public class Constants {
    public static final String loginURL = "https://api.backtory.com/auth/login";
    public static final String queryDriverURL = "https://api.backtory.com/object-storage/classes/query/Drivers";
    public static final String X_Backtory_Object_Storage_Id = "5a7591ede4b0f524934070f8";
    public static final String X_Backtory_Authentication_Id = "5a7591ece4b0f524934070e9";
    public static final String X_Backtory_Authentication_Key = "5a7591ece4b0135d84925b10";
    public static final String defaultFont = "ir_sans.ttf";
    public static WebInterFace webLoginUtil = RetrofitServiceGenerator.createServiceForAuthentication(WebInterFace.class);
    public static WebInterFace webDriverListUtil = RetrofitServiceGenerator.createServiceQueryWithAuthorization(WebInterFace.class);
//    public static WebInterFace webDriverSummeryUtil = RetrofitServiceGenerator.createServiceQueryWithAuthorization(WebInterFace.class);
}
