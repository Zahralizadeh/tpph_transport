package com.tpph.transport.driverDetails

import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm

/**
 * Created by zahra on 3/28/2018.
 */
 interface driverDetailsContract {
    interface View{
        fun showDriversDetail(driver: DriverModelRealm?)
    }

    interface Presenter{
        fun attachView(view: View,personelNO: String)

    }

    interface Model{
        fun attachPresenter(presenter: Presenter)
        fun loadDriversDetailfromDB(personelNO: String): DriverModelRealm?
    }

}