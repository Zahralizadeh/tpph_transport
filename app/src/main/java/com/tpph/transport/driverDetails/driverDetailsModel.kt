package com.tpph.transport.driverDetails
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm
import io.realm.Realm

/**
 * Created by zahra on 3/28/2018.
 */
class driverDetailsModel : driverDetailsContract.Model {


    private var presenter: driverDetailsContract.Presenter? = null

    override fun attachPresenter(presenter: driverDetailsContract.Presenter) {
        this.presenter = presenter
    }

    override fun loadDriversDetailfromDB(personelNO: String): DriverModelRealm? {
        val realm: Realm = Realm.getDefaultInstance()
        return realm.where(DriverModelRealm::class.java).equalTo("personelNO",personelNO.toInt()).findFirst()
    }
}