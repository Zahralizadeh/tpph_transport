package com.tpph.transport.driverDetails

import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm

/**
 * Created by zahra on 3/28/2018.
 */
class driverDetailsPeresenter : driverDetailsContract.Presenter {
    val model: driverDetailsModel = driverDetailsModel()
    private var view: driverDetailsContract.View? =null

    override fun attachView(view: driverDetailsContract.View, personelNO: String) {
        this.view = view
        model.attachPresenter(this)
        val driver: DriverModelRealm? = model.loadDriversDetailfromDB(personelNO)
        view.showDriversDetail(driver)


    }
}