package com.tpph.transport.driverDetails

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tpph.transport.BaseActivity
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm
import com.tpph.transport.PublicMethods
import com.tpph.transport.R
import com.tpph.transport.drivers.DriversContract
import kotlinx.android.synthetic.main.activity_driver_detail.*

class driverDetailActivity : BaseActivity(), driverDetailsContract.View {

    val presenter: driverDetailsPeresenter = driverDetailsPeresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_detail)
        val personnelNO: String = intent.getStringExtra("personnel")
        presenter.attachView(this, personnelNO)

    }

    override fun showDriversDetail(driver: DriverModelRealm?) {
        image.load(driver?.picURL)
        name.text(driver?.name + " " + driver?.family)
        car.text(PublicMethods.getString(this, R.string.car_title)
                + " " + driver?.carModel + " " + driver?.carColor)
        personelNO.text(PublicMethods.getString(this, R.string.personnelNO_title)
                + " " + driver?.personelNO.toString())

//        PublicMethods.showToast(this,personnelNO)
    }
}
