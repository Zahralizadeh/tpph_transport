package com.tpph.transport.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tpph.transport.HomeMap.pojoModels.DriversSummeryPojoModel;
import com.tpph.transport.HomeMap.pojoModels.RealmModels.DriverModelRealm;
import com.tpph.transport.R;
import com.tpph.transport.utils.views.myImageView;
import com.tpph.transport.utils.views.myTextView;
import com.tpph.transport.webModels.DriversListPojoModel;

import java.util.List;

/**
 * Created by Q & Z on 2/12/2018.
 */

public class driversListAdapter extends RecyclerView.Adapter<driversListAdapter.MyHolder> {
    private Context mContext;
    private DriversListPojoModel drivers;
    private DriversSummeryPojoModel driversSummery;
    private List<DriverModelRealm> savedDrivers;

    public driversListAdapter(Context mContext, List<DriverModelRealm> savedDrivers) {
        this.mContext = mContext;
        this.savedDrivers = savedDrivers;
    }

    public driversListAdapter(Context mContext, DriversSummeryPojoModel driversSummery) {

        this.mContext = mContext;
        this.driversSummery = driversSummery;
    }

    public driversListAdapter(Context mContext, DriversListPojoModel drivers) {
        this.mContext = mContext;
        this.drivers = drivers;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.driver_item, parent, false);

        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        String name = "";
        String car = "";
        String pic = "";
        if (drivers != null) {
            name = drivers.getResults().get(position).getName() + " " +
                    drivers.getResults().get(position).getFamily();
            car = drivers.getResults().get(position).getCarModel() + " " +
                    drivers.getResults().get(position).getCarColor();
            pic = drivers.getResults().get(position).getPicURL();
        } else if (driversSummery != null) {
            name = driversSummery.getResults().get(position).getName() + " " +
                    driversSummery.getResults().get(position).getFamily();
            car = driversSummery.getResults().get(position).getCarModel() + " " +
                    driversSummery.getResults().get(position).getCarColor();
            pic = driversSummery.getResults().get(position).getPicURL();
        } else if (savedDrivers != null) {
            name = savedDrivers.get(position).getName() + " " +
                    savedDrivers.get(position).getFamily();
            car = savedDrivers.get(position).getCarModel() + " " +
                    savedDrivers.get(position).getCarColor();
            pic = savedDrivers.get(position).getPicURL();
        }

        holder.image.load(pic);
        holder.name.setText(name);
        holder.car.setText(car);
    }

    @Override
    public int getItemCount() {
        if (drivers != null) {
            return drivers.getResults().size();
        } else if (driversSummery != null) {
            return driversSummery.getResults().size();
        } else if (savedDrivers != null) {
            return savedDrivers.size();
        }
        return 0;
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        myImageView image;
        myTextView name;
        myTextView car;

        public MyHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            car = itemView.findViewById(R.id.car);
        }
    }


}
