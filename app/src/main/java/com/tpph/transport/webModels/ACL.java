
package com.tpph.transport.webModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ACL {

    @SerializedName("All")
    @Expose
    private All all;

    public All getAll() {
        return all;
    }

    public void setAll(All all) {
        this.all = all;
    }

}
