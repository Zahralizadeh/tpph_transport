package com.tpph.transport.drivers;

import com.tpph.transport.login.loginContract;
import com.tpph.transport.webModels.DriversListPojoModel;

import java.util.List;

/**
 * Created by Q & Z on 2/11/2018.
 */

public interface DriversContract {

    interface View {
        void showDriverList(DriversListPojoModel drivers);
        void showToast(String msg);
        void showToast(int msg);

    }

    interface Presenter {
        void attachView(View view);
        void onFailureLoadDrivers(String response);
        void onSuccessLoadDrivers(DriversListPojoModel drivers);
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        String getToken(String key);
        void getDriversList();

    }

}
