package com.tpph.transport.drivers;

import com.tpph.transport.PublicMethods;
import com.tpph.transport.R;
import com.tpph.transport.webModels.DriversListPojoModel;

import java.util.List;

/**
 * Created by Q & Z on 2/11/2018.
 */

public class DriversPresenter implements DriversContract.Presenter {
    DriversModel model = new DriversModel();
    private DriversContract.View view;

    @Override
    public void attachView(DriversContract.View view) {
        this.view = view;
        model.attachPresenter(this);
        model.getDriversList();
    }

    @Override
    public void onFailureLoadDrivers(String response) {
        if (response.equalsIgnoreCase("invalid_token")) {
            view.showToast(R.string.errorInvalidToken);
        } else {
            view.showToast(R.string.errorConnectingToServers);
        }
    }

    @Override
    public void onSuccessLoadDrivers(DriversListPojoModel drivers) {
        view.showDriverList(drivers);
    }
}
