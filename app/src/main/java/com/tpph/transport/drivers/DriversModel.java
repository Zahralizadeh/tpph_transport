package com.tpph.transport.drivers;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;
import com.tpph.transport.Constants;
import com.tpph.transport.webModels.DriversListPojoModel;
import com.tpph.transport.webModels.ErrorDriversListPojoModel;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Q & Z on 2/11/2018.
 */

public class DriversModel implements DriversContract.Model {
    private DriversContract.Presenter presenter;

    @Override
    public void attachPresenter(DriversContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public String getToken(String key) {
        return Hawk.get(key);
    }

    @Override
    public void getDriversList() {
//        Constants.webDriverListUtil.getDriversList().enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                String responseStr = RepairACLstar(response.body());
//                Log.d("Driver", responseStr);
//                DriversListPojoModel drivers = getDriverList(responseStr);
//                presenter.onSuccessLoadDrivers(drivers);
//            }

//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                presenter.onFailureLoadDrivers(t.getMessage());
//            }
//        });

    }

    private String errorGetDriversList(String jsonStr) {
        Gson gson = new Gson();
        ErrorDriversListPojoModel error = gson.fromJson(jsonStr, ErrorDriversListPojoModel.class);
        return error.getError();
    }

    private String RepairACLstar(String responseString) {
        return responseString.replace("*", "All");
    }

    private DriversListPojoModel getDriverList(String jsonStr) {
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, DriversListPojoModel.class);
    }

}
