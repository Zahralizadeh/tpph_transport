package com.tpph.transport.drivers;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.orhanobut.hawk.Hawk;
import com.tpph.transport.BaseActivity;
import com.tpph.transport.PublicMethods;
import com.tpph.transport.R;
import com.tpph.transport.adapters.driversListAdapter;
import com.tpph.transport.webModels.DriversListPojoModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_drivers)
public class DriversActivity extends BaseActivity implements DriversContract.View {

    String token = Hawk.get("accessToken");
    DriversPresenter presenter = new DriversPresenter();

    @ViewById
    RecyclerView driversList;

    @AfterViews
    void initView() {
        presenter.attachView(this);

    }

    @Override
    public void showDriverList(DriversListPojoModel drivers) {
        driversListAdapter adapter = new driversListAdapter(mContext, drivers);
        driversList.setAdapter(adapter);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(mContext);
        driversList.setLayoutManager(lm);
    }

    @Override
    public void showToast(String msg) {
        PublicMethods.showToast(mContext, msg);
    }

    @Override
    public void showToast(int msg) {
        PublicMethods.showToast(mContext, PublicMethods.getString(mContext, msg));
    }
}
