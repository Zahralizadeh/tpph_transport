package com.tpph.transport;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Q & Z on 2/4/2018.
 */

public class PublicMethods {
    public static void showToast(Context mContext , String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getString (Context mContext, int resource){
        return mContext.getResources().getString(resource);
    }

    public static String RetrieveToken() {
        return Hawk.get("accessToken");
    }
}
